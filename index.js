const express = require('express')
const app = express()
const knex = require('knex')
const cookieParser = require('cookie-parser')
const cors = require('cors')

let config = require('./knexfile.js')
let db = knex(config.development)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())

//ALLOW CORS
/* var corsOptions = {
  origin: 'http://localhost:3060',
  credentials: true,
  optionsSuccessStatus: 200
} */
var whitelist = ['http://localhost:3060/', 'http://livres.formationfullstack.fr/', 'livres.formationfullstack.fr/']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  credentials: true
}
app.use(cors(corsOptions))

//MIDDLEWARES
const createAccount = require('./middlewares/createaccount')
const loginTo = require('./middlewares/loginto')
const insertbook = require('./middlewares/insertbook')
const searchbook = require('./middlewares/searchbook')
const userdata = require('./middlewares/userdata')
const updateuser_info = require('./middlewares/updateuser_info')
const updateuser_password = require('./middlewares/updateuser_password')
const updateuser_address = require('./middlewares/updateuser_address')
const getbookshelf = require('./middlewares/getbookshelf')
const updatecopy = require('./middlewares/updatecopy')
const deletecopy = require('./middlewares/deletecopy')

// --------------------------USER
app.post('/api/create-account', createAccount(db))
app.post('/api/login', loginTo(db))
app.get('/api/userdata', userdata(db))
app.put('/api/update-user-info', updateuser_info(db))
app.put('/api/update-user-password', updateuser_password(db))
app.put('/api/update-user-address', updateuser_address(db))

// --------------------------BOOK
app.get('/api/search-book', searchbook(db))
app.post('/api/addbook', insertbook(db))
app.get('/api/bookshelf', getbookshelf(db))

// --------------------------BOOKCOPY
app.put('/api/update-copy', updatecopy(db))
app.delete('/api/delete-copy', deletecopy(db))

// --------------------------API FOR ADMIN
//book
app.get('/api/book', (req, res) => {
  db.select('*').from('Book')
    .then((data) => {
      res.json(data)
    })
})
app.get('/api/book/:id', (req, res) => {
  db.select('*').where('id', req.params.id).from('Book')
    .then((data) => {
      res.json(data)
    })
})

//bookcopy
app.get('/api/bookcopies', (req, res) => {
  db.select('*').from('Bookcopy')
    .then((data) => {
      res.json(data)
    })
})
app.get('/api/bookcopies/:id', (req, res) => {
  db.select('*').where('id', req.params.id).from('Bookcopy')
    .then((data) => {
      res.json(data)
    })
})
app.get('/api/bookcopies/:user_id', (req, res) => {
  db.select('*').where('user_id', req.params.user_id).from('Bookcopy')
    .then((data) => {
      res.json(data)
    })
})

//user
/* app.get('/api/user', (req, res) => {
  db.select('*').from('User')
    .then((data) => {
      res.json(data)
    })
})
app.get('/api/user/:id', (req, res) => {
  db.select('*').where('id', req.params.id).from('User')
    .then((data) => {
      res.json(data)
    })
}) */

//LAUNCH SERVER
const port = process.env.PORT || 3060
app.listen(port, function () {
  console.log(`Server listening on port ${port}`)
})
