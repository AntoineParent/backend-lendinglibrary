exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('Book').del()
    .then(function () {
      // Inserts seed entries
      return knex('Book').insert([
        {
          id: 1,
          authors: "Ellie Quigley",
          description: "This is the definitive JavaScript tutorial for the serious nonprogrammer who is interested in mastering the full power of the language. Includes hundreds of example JavaScript programs that demonstrate both the fun and practical aspects.",
          isbn_10: "0131401629",
          isbn_13: "9780131401624",
          language: "en",
          pageCount: 730,
          publishedDate: "2004",
          publisher: "Prentice Hall Professional",
          textSnippet: "JovoScript Core Objects W 9.1 9.2 What Are Core Objects? Like an apple, <br>\n<b>JavaScript</b> has a core, and at its core are objects. Everything you do in <b>JavaScript</b> <br>\nwill be based on objects; you may create your own or use <b>JavaScript&#39;s</b> core <br>\nobjects,&nbsp;...",
          thumbnail: "http://books.google.com/books/content?id=IAJv2N1n9JAC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
          title: "JavaScript by Example"
        },
        {
          id: 2,
          authors: "Mark Hammond, Andy Robinson",
          description: "A demonstration of Python's basic technologies showcases the programming language's possiblities as a Windows development and administration tool.",
          isbn_10: "1565926218",
          isbn_13: "9781565926219",
          language: "en",
          pageCount: 652,
          publishedDate: "2000",
          publisher: "\"O'Reilly Media, Inc.\"",
          textSnippet: "Appendixes Key <b>Python</b> Modules and Functions The <b>Python</b> library is huge. This <br>\nis the Title of the Book, eMatter Edition This is the Title of the Book, eMatter <br>\nEdition. Copyright © 2008 O&#39;Reilly &amp; Associates, Inc. All rights reserved. IV.",
          thumbnail: "http://books.google.com/books/content?id=fzUCGtyg0MMC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
          title: "Python Programming On Win32"
        },
        {
          id: 3,
          authors: "David Weinachter",
          description: "David Weinachter est un ingénieur informatique qui a toujours été passionné par la programmation : c'est pour transmettre sa passion à ses enfants qu'il a créé Kidscod.in, la première méthode en ligne pour apprendre aux enfants à coder en toute autonomie, dès qu'ils commencent à savoir lire. Convaincu qu'on apprend mieux en s'amusant. David s'attache à faire découvrir le code aux enfants via des histoires interactives ou des jeux vidéo à créer.[Payot]",
          isbn_10: "221214332X",
          isbn_13: "9782212143324",
          language: "fr",
          pageCount: 63,
          publishedDate: "2015",
          publisher: "Editions Eyrolles",
          textSnippet: "David Weinachter est un ingénieur informatique qui a toujours été passionné par la programmation : c&#39;est pour transmettre sa passion à ses enfants qu&#39;il a créé Kidscod.in, la première méthode en ligne pour apprendre aux enfants à ...",
          thumbnail: "http://books.google.com/books/content?id=mLZRCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
          title: "Cahier d'activités Python pour les kids"
        },
        {
          id: 4,
          authors: "Jean-Noël Beury",
          description: "Cet ouvrage est un recueil d'exercices qui correspondent au programme d'informatique de la première et de la seconde année de toutes les filières : MPSI, PCSI, PTSI, MP, PC, PSI et PT. Le langage retenu est le langage Python. Chacun des 58 exercices comporte un corrigé détaillé avec une explication de la méthode résolution. La majorité des exercices concerne les élèves de première et de seconde année, sauf 28, qui sont repérés, et qui concernent uniquement ceux de seconde année. Les exercices sont en partie extraits de problèmes des concours 2015 à 2017.",
          isbn_10: "2100780050",
          isbn_13: "9782100780051",
          language: "fr",
          pageCount: 336,
          publishedDate: "2018-03-14",
          publisher: "Dunod",
          textSnippet: "Cet ouvrage est un recueil d&#39;exercices qui correspondent au programme d&#39;informatique de la première et de la seconde année de toutes les filières : MPSI, PCSI, PTSI, MP, PC, PSI et PT. Le langage retenu est le langage Python.",
          thumbnail: "http://books.google.com/books/content?id=jPBPDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
          title: "Informatique avec Python - Classes prépas scientifiques"
        }
      ]);
    });
};