exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('Bookcopy').del()
    .then(function () {
      // Inserts seed entries
      return knex('Bookcopy').insert([
        {
          id: 1,
          user_id: 3,
          book_id: 21,
          condition: 'Good',
          availability: true
        },
        {
          id: 2,
          user_id: 3,
          book_id: 22,
          condition: 'Damaged',
          availability: true
        },
        {
          id: 3,
          user_id: 1,
          book_id: 3,
          condition: 'Excellent',
          availability: false
        },
        {
          id: 4,
          user_id: 1,
          book_id: 4,
          condition: 'Good',
          availability: true
        },
        {
          id: 5,
          user_id: 3,
          book_id: 5,
          condition: 'Good',
          availability: false
        },
        {
          id: 6, //Zemmour
          user_id: 4, //Antoine
          book_id: 48,
          condition: 'Good',
          availability: false
        },
        {
          id: 7, //Zemmour
          user_id: 3, //Andres
          book_id: 47,
          condition: 'Good',
          availability: false
        },
        {
          id: 8,//Zemmour
          user_id: 4, //Antoine
          book_id: 46,
          condition: 'Good',
          availability: false
        },
        {
          id: 9,//Zemmour
          user_id: 1, //Dorine
          book_id: 45,
          condition: 'Good',
          availability: false
        },
        {
          id: 10,//Zemmour
          user_id: 1, //Dorine
          book_id: 44,
          condition: 'Good',
          availability: false
        },
        {
          id: 11,//Zemmour
          user_id: 2, //Tristan
          book_id: 43,
          condition: 'Good',
          availability: false
        },
        {
          id: 12,//Zemmour
          user_id: 3, //Andres
          book_id: 42,
          condition: 'Good',
          availability: false
        }
      ]);
    });
};