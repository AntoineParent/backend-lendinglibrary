exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('User').del()
    .then(function () {
      // Inserts seed entries
      return knex('User').insert([
        {
          id: 1,
          username: 'Dorine',
          email: 'dorine.berton@gmail.com',
          password: 'test',
          first_name: 'Dorine',
          last_name: 'Berton',
          zipcode: '75000',
          town: 'Paris',
          phone: '0601020304',
          is_active: true,
          is_admin: false,
          lat: 48.864716,
          long: 2.349014
        },
        {
          id: 2,
          username: 'Tristan',
          email: 'tristan.lambert@gmail.com',
          password: 'test',
          first_name: 'Tristan',
          last_name: 'Lambert',
          zipcode: '33000',
          town: 'Bordeaux',
          phone: '0601020304',
          is_active: true,
          is_admin: false,
          lat: 44.84044,
          long: -0.5805
        },
        {
          id: 3,
          username: 'Andres',
          email: 'andres.gomez@gmail.com',
          password: 'test',
          first_name: 'Andres',
          last_name: 'Gomez',
          zipcode: '13000',
          town: 'Toulouse',
          phone: '0601020304',
          is_active: true,
          is_admin: false,
          lat: 43.604500,
          long: 1.444000
        },
        {
          id: 4,
          username: 'Antoine',
          email: 'antoine.parent@gmail.com',
          password: 'test',
          first_name: 'Antoine',
          last_name: 'Parent',
          zipcode: '69000',
          town: 'Lyon',
          phone: '0642493287',
          is_active: true,
          is_admin: false,
          lat: 45.763420,
          long: 4.834277
        }
      ]);
    });
};