
exports.up = function (knex, Promise) {
  return knex.schema
    .createTable('User', table => {
      table.increments('id').unsigned().primary()
      table.string('username').notNull()
      table.string('email').unique().notNullable()
      table.string('password')
      table.string('first_name').notNullable()
      table.string('last_name').notNullable()
      table.string('zipcode').notNullable()
      table.string('town').notNullable()
      table.string('phone', 10).notNullable()
      table.timestamp('created_at').defaultTo(knex.fn.now())
      table.boolean('is_active').defaultTo(false)
      table.boolean('is_admin').defaultTo(false)
      table.real('lat')
      table.real('long')
    })
    .createTable('Book', table => {
      table.increments('id').unsigned().primary()
      table.string('title').notNull()
      table.string('authors').notNull()
      table.text('description')
      table.text('textSnippet')
      table.string('thumbnail')
      table.string('isbn_10', 10).unique().notNullable()
      table.string('isbn_13', 13).unique().notNullable()
      table.string('language', 2)
      table.string('pageCount')
      table.string('publishedDate')
      table.string('publisher')
      table.timestamp('created_at').defaultTo(knex.fn.now())
    })
    .createTable('Bookcopy', table => {
      table.increments('id').unsigned().primary()
      table.integer('book_id').references('id').inTable('Book').unsigned().notNullable().onDelete('cascade').index()
      table.integer('user_id').references('id').inTable('User').notNullable().onDelete('cascade')
      table.string('condition').defaultTo('Good')
      table.boolean('availability').defaultTo(true)
      table.timestamp('created_at').defaultTo(knex.fn.now())
    })
};
exports.down = function (knex, Promise) {

};
