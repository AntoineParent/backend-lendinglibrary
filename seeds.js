exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('User').del()
    .then(function () {
      // Inserts seed entries
      return knex('User').insert([
        {
          id: 1,
          username: 'Dorine',
          email: 'dorine.berton@gmail.com',
          password: 'test',
          first_name: 'Dorine',
          last_name: 'Berton',
          zipcode: '31380',
          town: 'Montastruc la conseillère',
          phone: '0601020304',
          is_active = true,
          role: 'user'
        },
        {
          id: 2,
          username: 'Tristan',
          email: 'tristan.lambert@gmail.com',
          password: 'test',
          first_name: 'Tristan',
          last_name: 'Lambert',
          zipcode: '31700',
          town: 'Blagnac',
          phone: '0601020304',
          is_active = true,
          role: 'user'
        },
        {
          id: 3,
          username: 'Andres',
          email: 'andres.gomez@gmail.com',
          password: 'test',
          first_name: 'Andres',
          last_name: 'Fomez',
          zipcode: '31000',
          town: 'Toulouse',
          phone: '0601020304',
          is_active = true,
          role: 'user'
        },
        {
          id: 4,
          username: 'Antoine',
          email: 'antoine.parent@gmail.com',
          password: 'test',
          first_name: 'Antoine',
          last_name: 'Parent',
          zipcode: '31570',
          town: 'Tarabel',
          phone: '0642493287',
          is_active = true,
          role: 'user'

        }
      ]);
    });
};