const JWT_SIGN_SECRET = 'hhxEph4B8xHzQe3TbTsBmTXSVLJCWHnZPpct9AbYxRSqpXEcs2EtJLeYy2KN7gKa'
const JWT_SESSION_SECRET = 'fAkdeVWsWye5VWP3NXNUjQdmYhqpd3Y63qyNua3ks2yWmccL4zg7fPhXGv5V2kD3'

const secret = (type) => {
    if(type === 'sign')
        return JWT_SIGN_SECRET

    if(type === 'session')
        return JWT_SESSION_SECRET
}

module.exports = secret