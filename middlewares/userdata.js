// Imports
const jwt = require('jsonwebtoken')
const secret = require('../utils/secret')

console.log(secret)
module.exports = (db) => async (req, res, next) => {
    //Recover cookie
    let user_id
    try {
        let decoded = jwt.verify(req.cookies.app_session, secret('session'))
        user_id = decoded.id
    } catch (err) {
        return res.status(401).send('Invalid token')
    }

    //Check user
    try {
        //let [user] = await db.select('id, username, email, phone, first_name, last_name, town, zipcode')
        let [user] = await db.select('*')
            .from('User').where('id', '=', user_id)
        res.status(200).send(user)
    } catch (err) {
        return res.status(500).send('Authorization : fail')
    }
}