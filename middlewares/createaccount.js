// Imports
const nodemailer = require('nodemailer')
const geocoder = require('geocoder-fr')
const bcrypt = require('bcrypt')

module.exports = (db) => (req, res) => {
  //DEFAULT VALUES
  const is_active = true
  const is_admin = false

  //FRONT DATA
  const { username, email, password, first_name, last_name, town, zipcode, phone } = req.body

  //PASSWORD CRYPT
  let cryptPassword = bcrypt.hashSync(password, 10)

  //GEOCODING
  geocoder.geocode(`${zipcode} ${town}`)
    .then(addr => {
      //console.log(addr.features[0].geometry)
      const lat = addr.features[0].geometry.coordinates[1]
      const long = addr.features[0].geometry.coordinates[0]
      insertDB(lat, long)
    })
    .catch((err) => {
      res.status(400).send({ message: err })
    })

  //INSERT DB
  const insertDB = (lat, long) => {
    //console.log(lat, long)
    db.into('User').insert({ username, email, password: cryptPassword, first_name, last_name, town, zipcode, phone, is_active, is_admin, lat, long })
      .then(() => res.sendStatus(200))
      .catch((err) => res.status(500).send({ message: err }))
  }
  //SENDING ACTIVATION MAIL
  /*const signToken = jwt.sign({ email: typeEmail }, secret('sign'))
  const mess_text = ``
  const mess_html = `<!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Lending Library - Account activation</title>
  </head>
  <body>
      <h1>Confirm your registration</h1>
      <p>To activate your account, please follow the link below</p>
      <p><a href="/confirm-registration?token=${signToken}">click here</a> to confirm</p>
  </body>
  </html>`
  const mail = {
      from: "contact@lendinglibrary.com",
      to: `${email}`,
      subject: "Lending library - Acccount activation",
      text: `${mess_text}`,
      html: `${mess_html}`
  };*/
}
