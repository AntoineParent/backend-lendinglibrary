
module.exports = (db) => (req, res) => {
  const { id, condition, availability } = req.body
  console.log(id)
  db('Bookcopy').where('id', '=', id)
    .update({ condition: condition, availability: availability })
    .then(() => res.sendStatus(200))
    .catch((err) => res.status(500).send({ message: err }))
}