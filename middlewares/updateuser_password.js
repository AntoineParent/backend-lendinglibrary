const bcrypt = require('bcrypt')

module.exports = (db) => (req, res) => {
  const { id, password } = req.body
  let cryptPassword = bcrypt.hashSync(password, 10)

  db('User').where('id', '=', id)
    .update({ password: cryptPassword })
    .then(() => res.sendStatus(200))
    .catch((err) => res.status(500).send({ message: err }))
}