module.exports = (db) => (req, res) => {
  //console.log('delete copy !', req.body.id)
  db('Bookcopy').where('id', '=', req.body.id)
    .del()
    .then(() => res.sendStatus(200))
    .catch((err) => res.status(500).send({ message: err }))
}