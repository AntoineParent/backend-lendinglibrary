// Imports
const jwt = require('jsonwebtoken')
const secret = require('../utils/secret')

module.exports = (db) => async (req, res) => {
  //RECOVER USER_ID FROM COOKIE
  let user_id
  try {
    let decoded = jwt.verify(req.cookies.app_session, secret('session'))
    user_id = decoded.id
  } catch (err) {
    return res.status(401).send('Invalid token')
  }

  //INSERT IN DB
  const { title, authors, description, textSnippet, thumbnail, isbn_10, isbn_13, language, pageCount, publishedDate, publisher, condition, availability } = req.body

  const insertBook = () => {
    db.insert({ title, authors, description, textSnippet, thumbnail, isbn_10, isbn_13, language, pageCount, publishedDate, publisher })
      .into('Book')
      .then(res => {
        console.log('Book inserted')
        insertCopy(res[0])
      })
      .catch(err => console.log(err))
  }

  const insertCopy = (id) => {
    db.insert({ user_id, book_id: id, condition: condition, availability: availability })
      .into('Bookcopy')
      .then(() => {
        console.log('Copy inserted')
        res.status(200).send({ inserted: true })
      })
      .catch(err => console.log(err))
  }

  //select
  try {
    let [wishedBook] = await db.select('id').into('Book').where('isbn_13', isbn_13)
    insertCopy(wishedBook.id)
  }
  catch {
    insertBook()
  }
}