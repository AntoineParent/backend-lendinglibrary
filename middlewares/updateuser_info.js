module.exports = (db) => (req, res) => {
  const { id, username, first_name, last_name, email, phone } = req.body
  //console.log(id, username, first_name, last_name, zipcode, email, phone, town)

  db('User').where('id', '=', id)
    .update({
      username, first_name, last_name, email, phone
    })
    .then(() => res.sendStatus(200))
    .catch((err) => res.status(500).send({ message: err }))
}