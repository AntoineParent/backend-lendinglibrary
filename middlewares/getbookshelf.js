// Imports
const jwt = require('jsonwebtoken')
const secret = require('../utils/secret')

module.exports = (db) => (req, res) => {
  //RECOVER USER_ID FROM COOKIE
  let user_id
  try {
    let decoded = jwt.verify(req.cookies.app_session, secret('session'))
    user_id = decoded.id
  } catch (err) {
    return res.status(401).send('Invalid token')
  }

  //BUILD OBJECT WITH book AND copy AS CHILDREN 
  const formatData = results => {
    let bookList = []
    results.map(data => {
      const { id, book_id, user_id, condition, availability, created_at, title, authors, description, textSnippet, thumbnail, isbn_10, isbn_13, language, pageCount, publishedDate, publisher } = data
      let exemp = {
        book: { title, description, authors, thumbnail, textSnippet, isbn_10, isbn_13, language, pageCount, publishedDate, publisher },
        copy: { id, book_id, user_id, condition, availability, created_at },
      }
      bookList.push(exemp)
    })
    return bookList
  }

  //SEARCH IN ONE QUERY => Copy and Book data are mixed
  try {
    db.select('*').from('Bookcopy')
      .where('user_id', '=', user_id)
      .join('Book', 'Bookcopy.book_id', '=', 'Book.id')
      .then(data => {
        res.json(formatData(data))
      })
      .catch((err) => res.status(503).send(err))
  } catch {

  }
}