// Imports
const bcrypt = require('bcrypt')
const secret = require('../utils/secret')
const jwt = require('jsonwebtoken')

module.exports = (db) => async (req, res) => {
    const email = req.body.email
    const password = req.body.password
    try {
        let user = await db('User').where({ email: email }).select('*')

        if (user.length === 0)
            return res.status(400).send({ error: 10 }) //'Email unknown' 

        if (user[0].active === 0)
            return res.status(400).send({ error: 20 }) //'Inactive account'

        if (!bcrypt.compareSync(password, (user[0]).password))
            return res.status(400).send({ error: 30 }) //'Incorrect password'

        let sessiontoken = jwt.sign({ id: user[0].id }, secret('session'))
        res.cookie('app_session', sessiontoken, { maxAge: 4200000 }) //10min --> 600s --> 600000ms
        return res.status(201).send({ cookie: 'cookie sent!' })

        //res.cookie('app_session', sessiontoken, { expires: new Date(Date.now() + 4200000) })
    } catch (err) {
        return res.status(500).send({ error: 50 }) //Internal server error
    }
}
