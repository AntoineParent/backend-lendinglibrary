const geocoder = require('geocoder-fr')

module.exports = (db) => (req, res) => {
  const { id, zipcode, town } = req.body
  //GEOCODING
  geocoder.geocode(`${zipcode} ${town}`)
    .then(addr => {
      const lat = addr.features[0].geometry.coordinates[1]
      const long = addr.features[0].geometry.coordinates[0]
      insertDB(lat, long)
    })
    .catch((err) => {
      res.status(400).send({ message: err })
      //console.log(err)
    })

  //INSERT DB
  const insertDB = (lat, long) => {
    db('User').where('id', '=', id)
      .update({ zipcode: zipcode, town: town, lat: lat, long: long })
      .then(() => res.sendStatus(200))
      .catch((err) => res.status(500).send({ message: err }))
  }
}