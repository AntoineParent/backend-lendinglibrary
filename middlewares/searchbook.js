module.exports = (db) => (req, res) => {
  const formatData = results => {
    let bookList = []
    results.map(data => {
      const { id, title, description, authors, thumbnail, textSnippet, isbn_10, isbn_13, language, pageCount, publishedDate, publisher, created_at, book_id, user_id, condition, availability, username, email, password, first_name, last_name, zipcode, town, phone, is_active, is_admin, lat, long } = data
      let exemp = {
        book: { title, description, authors, thumbnail, textSnippet, isbn_10, isbn_13, language, pageCount, publishedDate, publisher },
        copy: { created_at, book_id, user_id, condition, availability },
        user: { id, username, email, password, first_name, last_name, zipcode, town, phone, is_active, is_admin, lat, long }
      }
      bookList.push(exemp)
    })
    return bookList
  }
  //SEARCH IN ONE QUERY => Book, Copy, and User data are mixed
  db.select(
    'Book.*',
    'Bookcopy.*',
    'User.*'
  ).from('Book').where(`${req.query.option}`, 'like', `%${req.query.terms}%`)
    .join('Bookcopy', 'Book.id', '=', 'Bookcopy.book_id')
    .join('User', 'Bookcopy.user_id', '=', 'User.id')
    .then(data => {
      res.json(formatData(data))
    })
    .catch((err) => res.status(503).send(err))
}